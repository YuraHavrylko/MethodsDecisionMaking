﻿using System;

namespace WNG
{
    public class Item
    {
        public string Command { get; set; }
        public Action Handler { get; set; }

        public void DoSomething()
        {
            this.Handler.Invoke();
        }
    }
}