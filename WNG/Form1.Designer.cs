﻿namespace WNG
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameAltTxtBx1 = new System.Windows.Forms.TextBox();
            this.nameAltTxtBx2 = new System.Windows.Forms.TextBox();
            this.nameAltTxtBx3 = new System.Windows.Forms.TextBox();
            this.alt3TxtBx1 = new System.Windows.Forms.TextBox();
            this.alt2TxtBx1 = new System.Windows.Forms.TextBox();
            this.alt1TxtBx1 = new System.Windows.Forms.TextBox();
            this.alt3TxtBx2 = new System.Windows.Forms.TextBox();
            this.alt2TxtBx2 = new System.Windows.Forms.TextBox();
            this.alt1TxtBx2 = new System.Windows.Forms.TextBox();
            this.alt3TxtBx3 = new System.Windows.Forms.TextBox();
            this.alt2TxtBx3 = new System.Windows.Forms.TextBox();
            this.alt1TxtBx3 = new System.Windows.Forms.TextBox();
            this.alt3TxtBx4 = new System.Windows.Forms.TextBox();
            this.alt2TxtBx4 = new System.Windows.Forms.TextBox();
            this.alt1TxtBx4 = new System.Windows.Forms.TextBox();
            this.resltTxBx3 = new System.Windows.Forms.TextBox();
            this.resltTxBx2 = new System.Windows.Forms.TextBox();
            this.resltTxBx1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.minTxBx = new System.Windows.Forms.TextBox();
            this.maxTxBx = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.calc = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            this.selectAlgBx = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // nameAltTxtBx1
            // 
            this.nameAltTxtBx1.Location = new System.Drawing.Point(12, 41);
            this.nameAltTxtBx1.Name = "nameAltTxtBx1";
            this.nameAltTxtBx1.Size = new System.Drawing.Size(76, 20);
            this.nameAltTxtBx1.TabIndex = 0;
            // 
            // nameAltTxtBx2
            // 
            this.nameAltTxtBx2.Location = new System.Drawing.Point(12, 80);
            this.nameAltTxtBx2.Name = "nameAltTxtBx2";
            this.nameAltTxtBx2.Size = new System.Drawing.Size(76, 20);
            this.nameAltTxtBx2.TabIndex = 1;
            // 
            // nameAltTxtBx3
            // 
            this.nameAltTxtBx3.Location = new System.Drawing.Point(12, 118);
            this.nameAltTxtBx3.Name = "nameAltTxtBx3";
            this.nameAltTxtBx3.Size = new System.Drawing.Size(76, 20);
            this.nameAltTxtBx3.TabIndex = 2;
            // 
            // alt3TxtBx1
            // 
            this.alt3TxtBx1.Location = new System.Drawing.Point(126, 118);
            this.alt3TxtBx1.Name = "alt3TxtBx1";
            this.alt3TxtBx1.Size = new System.Drawing.Size(76, 20);
            this.alt3TxtBx1.TabIndex = 5;
            // 
            // alt2TxtBx1
            // 
            this.alt2TxtBx1.Location = new System.Drawing.Point(126, 80);
            this.alt2TxtBx1.Name = "alt2TxtBx1";
            this.alt2TxtBx1.Size = new System.Drawing.Size(76, 20);
            this.alt2TxtBx1.TabIndex = 4;
            // 
            // alt1TxtBx1
            // 
            this.alt1TxtBx1.Location = new System.Drawing.Point(126, 41);
            this.alt1TxtBx1.Name = "alt1TxtBx1";
            this.alt1TxtBx1.Size = new System.Drawing.Size(76, 20);
            this.alt1TxtBx1.TabIndex = 3;
            // 
            // alt3TxtBx2
            // 
            this.alt3TxtBx2.Location = new System.Drawing.Point(242, 118);
            this.alt3TxtBx2.Name = "alt3TxtBx2";
            this.alt3TxtBx2.Size = new System.Drawing.Size(76, 20);
            this.alt3TxtBx2.TabIndex = 8;
            // 
            // alt2TxtBx2
            // 
            this.alt2TxtBx2.Location = new System.Drawing.Point(242, 80);
            this.alt2TxtBx2.Name = "alt2TxtBx2";
            this.alt2TxtBx2.Size = new System.Drawing.Size(76, 20);
            this.alt2TxtBx2.TabIndex = 7;
            // 
            // alt1TxtBx2
            // 
            this.alt1TxtBx2.Location = new System.Drawing.Point(242, 41);
            this.alt1TxtBx2.Name = "alt1TxtBx2";
            this.alt1TxtBx2.Size = new System.Drawing.Size(76, 20);
            this.alt1TxtBx2.TabIndex = 6;
            // 
            // alt3TxtBx3
            // 
            this.alt3TxtBx3.Location = new System.Drawing.Point(362, 118);
            this.alt3TxtBx3.Name = "alt3TxtBx3";
            this.alt3TxtBx3.Size = new System.Drawing.Size(76, 20);
            this.alt3TxtBx3.TabIndex = 11;
            // 
            // alt2TxtBx3
            // 
            this.alt2TxtBx3.Location = new System.Drawing.Point(362, 80);
            this.alt2TxtBx3.Name = "alt2TxtBx3";
            this.alt2TxtBx3.Size = new System.Drawing.Size(76, 20);
            this.alt2TxtBx3.TabIndex = 10;
            // 
            // alt1TxtBx3
            // 
            this.alt1TxtBx3.Location = new System.Drawing.Point(362, 41);
            this.alt1TxtBx3.Name = "alt1TxtBx3";
            this.alt1TxtBx3.Size = new System.Drawing.Size(76, 20);
            this.alt1TxtBx3.TabIndex = 9;
            // 
            // alt3TxtBx4
            // 
            this.alt3TxtBx4.Location = new System.Drawing.Point(483, 118);
            this.alt3TxtBx4.Name = "alt3TxtBx4";
            this.alt3TxtBx4.Size = new System.Drawing.Size(76, 20);
            this.alt3TxtBx4.TabIndex = 14;
            // 
            // alt2TxtBx4
            // 
            this.alt2TxtBx4.Location = new System.Drawing.Point(483, 80);
            this.alt2TxtBx4.Name = "alt2TxtBx4";
            this.alt2TxtBx4.Size = new System.Drawing.Size(76, 20);
            this.alt2TxtBx4.TabIndex = 13;
            // 
            // alt1TxtBx4
            // 
            this.alt1TxtBx4.Location = new System.Drawing.Point(483, 41);
            this.alt1TxtBx4.Name = "alt1TxtBx4";
            this.alt1TxtBx4.Size = new System.Drawing.Size(76, 20);
            this.alt1TxtBx4.TabIndex = 12;
            // 
            // resltTxBx3
            // 
            this.resltTxBx3.Location = new System.Drawing.Point(610, 118);
            this.resltTxBx3.Name = "resltTxBx3";
            this.resltTxBx3.Size = new System.Drawing.Size(76, 20);
            this.resltTxBx3.TabIndex = 17;
            // 
            // resltTxBx2
            // 
            this.resltTxBx2.Location = new System.Drawing.Point(610, 80);
            this.resltTxBx2.Name = "resltTxBx2";
            this.resltTxBx2.Size = new System.Drawing.Size(76, 20);
            this.resltTxBx2.TabIndex = 16;
            // 
            // resltTxBx1
            // 
            this.resltTxBx1.Location = new System.Drawing.Point(610, 41);
            this.resltTxBx1.Name = "resltTxBx1";
            this.resltTxBx1.Size = new System.Drawing.Size(76, 20);
            this.resltTxBx1.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Альтернатива";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(126, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Вибір1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(242, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Вибір2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(362, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Вибір3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(483, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Вибір4";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(610, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "K mk";
            // 
            // minTxBx
            // 
            this.minTxBx.Location = new System.Drawing.Point(90, 190);
            this.minTxBx.Name = "minTxBx";
            this.minTxBx.Size = new System.Drawing.Size(76, 20);
            this.minTxBx.TabIndex = 24;
            // 
            // maxTxBx
            // 
            this.maxTxBx.Location = new System.Drawing.Point(90, 216);
            this.maxTxBx.Name = "maxTxBx";
            this.maxTxBx.Size = new System.Drawing.Size(76, 20);
            this.maxTxBx.TabIndex = 25;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "minij";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 216);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "maxij";
            // 
            // calc
            // 
            this.calc.Location = new System.Drawing.Point(221, 190);
            this.calc.Name = "calc";
            this.calc.Size = new System.Drawing.Size(146, 46);
            this.calc.TabIndex = 28;
            this.calc.Text = "Calc";
            this.calc.UseVisualStyleBackColor = true;
            this.calc.Click += new System.EventHandler(this.calc_Click);
            // 
            // exit
            // 
            this.exit.Location = new System.Drawing.Point(397, 190);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(145, 46);
            this.exit.TabIndex = 29;
            this.exit.Text = "Exit";
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // selectAlgBx
            // 
            this.selectAlgBx.FormattingEnabled = true;
            this.selectAlgBx.Items.AddRange(new object[] {
            "Метод Лапласа",
            "Метод Вальда",
            "Метод Свіджа",
            "Метод компромісу Гурвіца",
            "Метод половинного компромісу ризиків"});
            this.selectAlgBx.Location = new System.Drawing.Point(221, 154);
            this.selectAlgBx.Name = "selectAlgBx";
            this.selectAlgBx.Size = new System.Drawing.Size(217, 21);
            this.selectAlgBx.TabIndex = 30;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 262);
            this.Controls.Add(this.selectAlgBx);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.calc);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.maxTxBx);
            this.Controls.Add(this.minTxBx);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.resltTxBx3);
            this.Controls.Add(this.resltTxBx2);
            this.Controls.Add(this.resltTxBx1);
            this.Controls.Add(this.alt3TxtBx4);
            this.Controls.Add(this.alt2TxtBx4);
            this.Controls.Add(this.alt1TxtBx4);
            this.Controls.Add(this.alt3TxtBx3);
            this.Controls.Add(this.alt2TxtBx3);
            this.Controls.Add(this.alt1TxtBx3);
            this.Controls.Add(this.alt3TxtBx2);
            this.Controls.Add(this.alt2TxtBx2);
            this.Controls.Add(this.alt1TxtBx2);
            this.Controls.Add(this.alt3TxtBx1);
            this.Controls.Add(this.alt2TxtBx1);
            this.Controls.Add(this.alt1TxtBx1);
            this.Controls.Add(this.nameAltTxtBx3);
            this.Controls.Add(this.nameAltTxtBx2);
            this.Controls.Add(this.nameAltTxtBx1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nameAltTxtBx1;
        private System.Windows.Forms.TextBox nameAltTxtBx2;
        private System.Windows.Forms.TextBox nameAltTxtBx3;
        private System.Windows.Forms.TextBox alt3TxtBx1;
        private System.Windows.Forms.TextBox alt2TxtBx1;
        private System.Windows.Forms.TextBox alt1TxtBx1;
        private System.Windows.Forms.TextBox alt3TxtBx2;
        private System.Windows.Forms.TextBox alt2TxtBx2;
        private System.Windows.Forms.TextBox alt1TxtBx2;
        private System.Windows.Forms.TextBox alt3TxtBx3;
        private System.Windows.Forms.TextBox alt2TxtBx3;
        private System.Windows.Forms.TextBox alt1TxtBx3;
        private System.Windows.Forms.TextBox alt3TxtBx4;
        private System.Windows.Forms.TextBox alt2TxtBx4;
        private System.Windows.Forms.TextBox alt1TxtBx4;
        private System.Windows.Forms.TextBox resltTxBx3;
        private System.Windows.Forms.TextBox resltTxBx2;
        private System.Windows.Forms.TextBox resltTxBx1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox minTxBx;
        private System.Windows.Forms.TextBox maxTxBx;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button calc;
        private System.Windows.Forms.Button exit;
        private System.Windows.Forms.ComboBox selectAlgBx;
    }
}

