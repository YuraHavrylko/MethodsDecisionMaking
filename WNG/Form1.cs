﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WNG.Algoritms;

namespace WNG
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<double> alternative1;
        List<double> alternative2;
        List<double> alternative3;


        private void calc_Click(object sender, EventArgs e)
        {
            List<Item> list = new List<Item>();
            list.Add(new Item() { Command = "Метод Лапласа", Handler = Laplase});
            list.Add(new Item() { Command = "Метод Вальда", Handler = Wald });
            list.Add(new Item() { Command = "Метод Свіджа", Handler = Svidzh });
            list.Add(new Item() { Command = "Метод компромісу Гурвіца", Handler = HurwitzCompromise });
            list.Add(new Item() { Command = "Метод половинного компромісу ризиків", Handler = HalfCompromiseRisks });
            alternative1 = new List<double>();
            alternative2 = new List<double>();
            alternative3 = new List<double>();

            alternative1.Add(Convert.ToDouble(alt1TxtBx1.Text));
            alternative1.Add(Convert.ToDouble(alt1TxtBx2.Text));
            alternative1.Add(Convert.ToDouble(alt1TxtBx3.Text));
            alternative1.Add(Convert.ToDouble(alt1TxtBx4.Text));

            alternative2.Add(Convert.ToDouble(alt2TxtBx1.Text));
            alternative2.Add(Convert.ToDouble(alt2TxtBx2.Text));
            alternative2.Add(Convert.ToDouble(alt2TxtBx3.Text));
            alternative2.Add(Convert.ToDouble(alt2TxtBx4.Text));

            alternative3.Add(Convert.ToDouble(alt3TxtBx1.Text));
            alternative3.Add(Convert.ToDouble(alt3TxtBx2.Text));
            alternative3.Add(Convert.ToDouble(alt3TxtBx3.Text));
            alternative3.Add(Convert.ToDouble(alt3TxtBx4.Text));

            list.First(x => selectAlgBx.Text.Equals(x.Command)).DoSomething();

        }

        public void Laplase()
        {
            var algoritm = new Laplace();
            var arrayResult = new List<double>();

            arrayResult.Add(algoritm.calculate(alternative1));
            arrayResult.Add(algoritm.calculate(alternative2));
            arrayResult.Add(algoritm.calculate(alternative3));

            resltTxBx1.Text = Convert.ToString(arrayResult[0]);
            resltTxBx2.Text = Convert.ToString(arrayResult[1]);
            resltTxBx3.Text = Convert.ToString(arrayResult[2]);

            minTxBx.Text = Convert.ToString(algoritm.max(arrayResult));
        }

        public void Svidzh()
        {
            var algoritm = new Svidzh();

            var tempAlternative = new List<List<double>>();
            tempAlternative.Add(new List<double>(alternative1));
            tempAlternative.Add(new List<double>(alternative2));
            tempAlternative.Add(new List<double>(alternative3));

            var arrayResult = algoritm.calculate(tempAlternative);

            resltTxBx1.Text = Convert.ToString(arrayResult[0]);
            resltTxBx2.Text = Convert.ToString(arrayResult[1]);
            resltTxBx3.Text = Convert.ToString(arrayResult[2]);

            minTxBx.Text = Convert.ToString(algoritm.min(arrayResult));
        }

        public void HurwitzCompromise()
        {
            var algoritm = new HurwitzCompromise();

            var tempAlternative = new List<List<double>>();
            tempAlternative.Add(new List<double>(alternative1));
            tempAlternative.Add(new List<double>(alternative2));
            tempAlternative.Add(new List<double>(alternative3));

            var arrayResult = algoritm.calculate(tempAlternative);

            resltTxBx1.Text = Convert.ToString(arrayResult[0]);
            resltTxBx2.Text = Convert.ToString(arrayResult[1]);
            resltTxBx3.Text = Convert.ToString(arrayResult[2]);

            var temp = new List<double>();
            temp.AddRange(alternative1);
            temp.AddRange(alternative2);
            temp.AddRange(alternative3);

            minTxBx.Text = Convert.ToString(algoritm.min(temp));
            maxTxBx.Text = Convert.ToString(algoritm.max(temp));
        }

        public void HalfCompromiseRisks()
        {
            var algoritm = new HalfCompromiseRisks();

            var tempAlternative = new List<List<double>>();
            tempAlternative.Add(new List<double>(alternative1));
            tempAlternative.Add(new List<double>(alternative2));
            tempAlternative.Add(new List<double>(alternative3));

            var arrayResult = algoritm.calculate(tempAlternative);

            resltTxBx1.Text = Convert.ToString(arrayResult[0]);
            resltTxBx2.Text = Convert.ToString(arrayResult[1]);
            resltTxBx3.Text = Convert.ToString(arrayResult[2]);

            minTxBx.Text = Convert.ToString(algoritm.min(arrayResult));
        }

        public void Wald()
        {
            var algoritm = new Wald();

            var tempAlternative = new List<List<double>>();
            tempAlternative.Add(new List<double>(alternative1));
            tempAlternative.Add(new List<double>(alternative2));
            tempAlternative.Add(new List<double>(alternative3));

            var arrayResult = algoritm.calculate(tempAlternative);

            resltTxBx1.Text = Convert.ToString(arrayResult[0]);
            resltTxBx2.Text = Convert.ToString(arrayResult[1]);
            resltTxBx3.Text = Convert.ToString(arrayResult[2]);

            minTxBx.Text = Convert.ToString(algoritm.max(arrayResult));
        }



        private void exit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
