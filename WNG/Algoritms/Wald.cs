﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WNG.Algoritms
{
    public class Wald
    {
        public double min(List<double> list)
        {
            return list.Min();
        }
        public double max(List<double> list)
        {
            return list.Max();
        }

        public List<double> calculate(List<List<double>> list)
        {
            var arrayResult = new List<double>();

            for (int i = 0; i < list.Count; i++)
            {
                arrayResult.Add(min(list[i]));
            }
            return arrayResult;
        }
    }
}