﻿using System.Collections.Generic;
using System.Linq;

namespace WNG.Algoritms
{
    public class HurwitzCompromise
    {
        public double min(List<double> list)
        {
            return list.Min();
        }

        public double max(List<double> list)
        {
            return list.Max();
        }

        public List<double> calculate(List<List<double>> list)
        {
            var arrayResult = new List<double>();

            var arrayMax = new List<double>();
            var arrayMin = new List<double>();

            for (int i = 0; i < list.Count; i++)
            {
                arrayMax.Add(max(list[i]));
                arrayMin.Add(min(list[i]));
            }
            for (int i = 0; i < list.Count; i++)
            {
                arrayResult.Add((arrayMax[i] + arrayMin[i])/2);
            }
            return arrayResult;
        }
    }
}