﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WNG.Algoritms
{
    public class Svidzh
    {
        public double min(List<double> list)
        {
            return list.Min();
        }
        public double max(List<double> list)
        {
            return list.Max();
        }

        public List<double> calculate(List<List<double>> list)
        {
            var arrayResult = new List<double>();

            for (int i = 0; i < list.Count; i++)
            {
                arrayResult.Add(max(list[i]));
            }

            for (int i = 0; i < list.Count; i++)
            {
                for (int j = 0; j < list[i].Count; j++)
                {
                    list[i][j] = Math.Abs(list[i][j] - arrayResult[i]);
                }
            }

            for (int i = 0; i < list.Count; i++)
            {
                arrayResult[i] = max(list[i]);
            }

            return arrayResult;
        }

    }
}