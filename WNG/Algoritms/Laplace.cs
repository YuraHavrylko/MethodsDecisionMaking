﻿using System.Collections.Generic;
using System.Linq;

namespace WNG.Algoritms
{
    public class Laplace
    {
        public double calculate(List<double> list)
        {
            double result, sum = 0;
            for (int i = 0; i < list.Count; i++)
            {
                sum += list[i];
            }
            result = sum / list.Count;
            return result;
        }
        public double max(List<double> list)
        {
            return list.Max();
        }
    }
}